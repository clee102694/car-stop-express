from django.contrib import admin
from .models import Sale, Customer, Salesperson

#import VO object

# Register your models here.

@admin.register(Sale)
class SalesAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass
