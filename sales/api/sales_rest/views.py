from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale

# Create your views here.

# DOUBLE CHECK ENCODER ATTRIBUTESSSSSSSSSS

#create AutomobileVOEncoder,
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin", "sold", "href", "id",
    ]

#create show and list Salesperson Encoders
class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name", "last_name", "employee_id", "id",
    ]

    #encoders = {"automobile":AutomobileVOEncoder()}

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name", "last_name", "employee_id", "id"
    ]

    #encoders = {"automobile":AutomobileVOEncoder()}

#create show and list Customer Encoders

class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name", "last_name", "phone_number", "id"
    ]

    #encoders = {"automobile":AutomobileVOEncoder()}

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name", "last_name", "address", "phone_number", "id"
    ]

    #encoders = {"automobile":AutomobileVOEncoder()}

#create show and list Sales Encoders

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price", "automobile", "salesperson", "customer", "id"
    ]

    encoders = {"automobile":AutomobileVOEncoder(),
                "salesperson":SalespersonListEncoder(),
                "customer":CustomerListEncoder()}

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price", "automobile", "salesperson", "customer", "id"
    ]

    encoders = {"automobile":AutomobileVOEncoder(),
                "salesperson":SalespersonListEncoder(),
                "customer":CustomerListEncoder()}


#create show and list views for Salesperson, include encoder in the else return statements
@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method =="GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople":salespeople},
            encoder=SalespersonListEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesperson(request, id):
    if request.method=="GET":
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            {"salesperson":salesperson},
            encoder=SalespersonDetailEncoder,
            safe=False,
        )
    elif request.method=="DELETE":
        count, _=Salesperson.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # Revise this if necessary for edit capabilities
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

#create show and list views for Customer

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method=="GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer":customer},
            encoder=CustomerListEncoder,
            #safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, id):
    if request.method=="GET":
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            {"customer":customer},
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count, _=Customer.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )

#create show and list views for Sales

@require_http_methods(["GET", "POST"])
def api_list_sales(request, automobile_vin=None, customer_id=None, salespeep_id=None):
        if request.method == "GET":
            if automobile_vin is not None:
                automobile = AutomobileVO.objects.filter(vin=automobile_vin)
            else:
                automobile = AutomobileVO.objects.all()
            # return JsonResponse(
            #     {"automobile": automobile},
            #     encoder=AutomobileVOEncoder,
            #     )
            if customer_id is not None:
                customer = Customer.objects.filter(id=customer_id)
            else:
                customer = Customer.objects.all()
            # return JsonResponse(
            #     {"automobile": automobile},
            #     encoder=AutomobileVOEncoder,
            #     )
            if salespeep_id is not None:
                salesperson = Salesperson.objects.filter(employee_id=salespeep_id)
            else:
                salesperson = Salesperson.objects.all()

                JsonResponse( { "automobile": automobile, "customer": customer, "salesperson": salesperson }, encoder=[AutomobileVOEncoder, CustomerDetailEncoder, SalespersonDetailEncoder], safe=False )
            # return JsonResponse(
            #     {"automobile": automobile},
            #     encoder=AutomobileVOEncoder,)

            # return JsonResponse(
            #     {"customer": customer},
            #     encoder=CustomerDetailEncoder,
            # )
            # return JsonResponse(
            #     {"salesperson": salesperson},
            #     encoder=SalespersonDetailEncoder,
            #     safe=False

            # )

        else:
            #json_response_data = {"automobile": json.loads()}


            content = json.loads(request.body)

            # Get the Conference object and put it in the content dict
            try:
                automobile_vin = content["automobile_vin"]
                print(automobile_vin)
                automobile_href = f"http://localhost:8100/api/automobiles/{automobile_vin}/"
                print(automobile_href)
                automobile = AutomobileVO.objects.get(href=automobile_href)
                print(automobile)
                content["automobile"] = automobile
                print(automobile)
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid vin"},
                    status=400,
                )
            try:
                customer_id = content["customer_id"]
                print(customer_id)
                #automobile_href = f"http://localhost:8100/api/automobiles/{automobile_vin}/"
                #print(automobile_href)
                customer = Customer.objects.get(id=customer_id)
                print(customer)
                content["customer"] = customer
                print(customer)
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid customer ID"},
                    status=400,
                )
            try:
                salesperson_id = content["salesperson_id"]
                print(salesperson_id)
                #automobile_href = f"http://localhost:8100/api/automobiles/{automobile_vin}/"
                #print(automobile_href)
                salesperson = Salesperson.objects.get(employee_id=salesperson_id)
                print(salesperson)
                content["salesperson"] = salesperson
                print(automobile)
            except Salesperson.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid employee ID"},
                    status=400,
                )


            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False,
            )
    # if request.method=="GET":
    #     sales = Sale.objects.all()
    #     return JsonResponse(
    #         {"sales":sales},
    #         encoder=SaleListEncoder,
    #         safe=False,
    #     )
    # else:
    #     try:
    #         content = json.loads(request.body)

    #         price = content["price"]
    #         print("should show price:", price)

    #         print("Shows the content:", content)
    #         salesperson_id = content["salesperson_id"] # how I name the variables in JSON/
    #         print("should show salesperson_id:", salesperson_id)
    #         salesperson = Salesperson.objects.get(employee_id=salesperson_id) # converting JSON data to Model object data field
    #         content["salesperson"] = salesperson
    #         print("should show salesperson:", salesperson)

    #         customer_id = content["customer_id"]
    #         print("should show customer_id:", customer_id)
    #         customer = Customer.objects.get(id=customer_id)
    #         content["customer"] = customer
    #         print("should show customer:", customer)


    #         try:
    #             automobile_vin = content["automobile_vin"]
    #             print("Should show Auto vin:", automobile_vin)
    #             automobile_href = f"http://localhost:8100/api/automobiles/{automobile_vin}/"

    #             automobile = AutomobileVO.objects.get(href=automobile_href)
    #             content["automobile"]=automobile # Failing here
    #             print("Should show Auto object:", automobile) # Failing here, does not print

    #         except AutomobileVO.DoesNotExist:
    #             print("did not find auto at that VIN:", automobile_vin)
    #             raise ValueError(f"no autos found at the given VIN: {automobile_vin}")


    #     except Exception as e:
    #         print("error message:", e)
    #         response = JsonResponse(
    #             {"message":"Could not create the sale"})
    #         response.status_code=400
    #         return response

        # sale = Sale.objects.create(**content) # Failing here.
        # print("Should show created object pre-encoder:", sale)
        # return JsonResponse(
        #         sale,
        #         encoder=SaleDetailEncoder,
        #         safe=False,
        #         )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_sale(request, id):
    if request.method=="GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )
    elif request.method=="DELETE":
        count, _=Sale.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    else:
        content = json.loads(request.body)
        try:
            if "customer" in content:
                customer = Customer.objects.get(id=content["customer"]) #change/research ID
                content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "customer does not exist"},
                status=400,
            )
