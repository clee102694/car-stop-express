from django.db import models
from django.urls import reverse


# Create your models here.

# Create a VO model
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False) #default to not sold for now.
    href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.vin}"

class Salesperson(models.Model):
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.employee_id}"

    def get_api_url(self):
        return reverse("api_show_salesperson", kwargs={"employee_id": self.id})


class Customer(models.Model):
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    address = models.TextField(null=True)
    phone_number = models.PositiveIntegerField() # look for better field for phone number?

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"id": self.id})


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_automobile",
        null=False,
        on_delete=models.CASCADE # maybe protect this? Come back to this.

    )   #foreign key fields except price
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales_salesperson",
        null=False,
        on_delete=models.CASCADE # or should this be protect?
    )
    customer =  models.ForeignKey(
        Customer,
        related_name="sales_customer",
        null=False,
        on_delete=models.CASCADE # or should this be protect?


    )
    price = models.DecimalField(max_digits=8, decimal_places=2) # look for something better maybe?

    def __str__(self):
        return f"{self.id}"
