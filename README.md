# CarCar

Team:

* Cameron Lee - Service micro-service
* Ramesh Beharry - Sales micro-service

## Design

How to use this product:

-Fork the directory from GitHub
-clone the directory into desired filepath
-launch Docker
-Open terminal and CD into desired filepath
-in your terminal run these commands:
    -docker volume create <<name>>
    -docker compose build
    -docker compose up
-wait for docker to finish creating and running the containers
    (this process can take a bit, so give it around 5 minutes)
-go to localhost:3000
-start setting up your new car business using our CarCar app!

## Service microservice

The Technicians model allows you to create, view information on, update* or delete* a technician
with the below attributes:
    -first name (max length of 30)
    -last name (max length of 30)
    -employee ID (must be unique per employee)

The Appointment model allows you to create, view information on, update* or delete*
an appointment with the below attributes:
    -Date
    -Time
    -Reason for visit (max length of 150)
    -Status of the appointment (finished, canceled, etc.)(max length of 30 and defaults to "created")
    -VIN (max length of 17)
    -Customer (max length of 150 for those really long names)
    -Technician (see above)

The AutomobileVO model allows our data to go into a polling service to grab data
from the inventory microservice on the automobile model every 60 seconds so we can
reference it within the models listed above; the attributes pulled are:
    -VIN (max length of 17)
    -Sold status (yes, no)

*note: to update or delete most attributes listed above, you will need to use Insomnia as these
features are not yet included on the website

ex:
    to delete a technician in insomnia, create a file that goes to http://localhost:8080/api/technicians/<int:id>/ with the "DELETE" method selected and replace <int:id> with the ID of the technician
    -
    to update a technician in insomnia, create a file that goes to http://localhost:8080/api/technicians/<int:id>/ with the "PUT" method selected and replace <int:id> with the ID of the technician
    switch the "body" dropdown to "JSON" and add fields in a JSON-fashion as such:
        {
        "first_name": "Roger",
        "last_name": "Wilson",
        "employee_id": "15"
        }

## Sales microservice

In designing the models for this project, we started by including all
the required attributes listed in the assignment instructions.

The attributes for this field are first and last names and an
employee ID number. I included Max length parameters in the text field to improve the
display of names when rendering the web page.

The employee ID number needs to be a unique number that corresponds to a single employee,
so the unique=true parameters were given to this attribute.

The Manufacturer

The Sale

The AutomobileVO is a value object (VO) model that is designed to
correspond with the Automobile model. This project is structered as a microservice,
and Sales is one such microservice. The idea is for sales to be compartmentalized.
This type of design creates resiliency in the application because if the service
microservice goes down, sales transactions can continue.

Because the Sales micro-service is siloed in this way from the "main" project application (Inventory),
the AutomobileVO sync data with the Inventory application through a poller.
The poller is a function that continuously checks for changes in the automobiles at the
Inventory app, on port 8000, as the specified URL for automobiles. If it detects a change,
it with either update the data record or create a new one. This is how the AutomobileVO
model is used to sync data with the Automobile model that lives in a separate app, which is on
a separate Docker container and accessed at a different URL port.


## URLs and Ports

React (the server) - http://localhost:3000/
Sales API - http://localhost:8080
Service API - http://localhost:8090
Inventory API - http://localhost:8100

all internals dealt with at port 8000
