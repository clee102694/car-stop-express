import { useEffect, useState } from 'react'


function ModelList() {
    const [models, setModels] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/models')

        if (response.ok) {
            const data = await response.json()
            setModels(data.models)

        }
    }
    useEffect(() => {
        getData()
    }, [])


    return (
        <>
            <h1>Vehicle Models</h1>
            <div className="row row-cols-3">
                {models.map(model => {
                    return (
                        <div key={model.id} className="card text-center mb-2 mt-5 shadow p-2 rounded">
                            <img src={model.picture_url} className="card-img-top" />
                            <div className="card-body">
                                <h5 className="card-title">{model.manufacturer.name}</h5>
                                <p className="card-text">{model.name}</p>
                            </div>
                        </div>
                    )
                })}
            </div>
        </>
    )
}

export default ModelList
