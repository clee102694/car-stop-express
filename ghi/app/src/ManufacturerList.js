import { useEffect, useState } from 'react'


function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/')

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div>
                <h1>Manufacturers</h1>
                <table className="table mt-5">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                        </tr>
                    </thead>
                        {manufacturers.map(manufacturer => {
                            return(
                                <tbody key={manufacturer.href}>
                                    <tr>
                                        <td>{manufacturer.name}</td>
                                    </tr>
                                </tbody>
                            )
                        })}
                </table>
            </div>
        </>
    )

}

export default ManufacturerList
