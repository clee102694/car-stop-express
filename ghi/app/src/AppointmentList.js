import { useEffect, useState } from 'react'


function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    // const [newStatusFinish, setNewStatusFinish] = useState('')
    // const [newStatusCancel, setNewStatusCancel] = useState('')
    const [filterStatus, setFilterStatus] = useState("created")



    const getData = async () => {
        const response = await fetch(`http://localhost:8080/api/appointments/`)

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    // const handleOnClick = async (event) => {
    //     event.preventDefault()

    //     const data = {}
    //     data.status = status <-- "status" is reserved AND deprecated, not enough time to reroute variable

    //     const statusURL = `http://localhost:8080/api/appointments/`

    //     const fetchConfig = {
    //         method: "POST",
    //         body: JSON.stringify(data),
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //     }

    //     const response = await fetch(statusURL, fetchConfig)

    //     if (response.ok) {
    //         const updatedStatus = await response.json()
    //         setFilterStatus('')
    //     }
    // }

    // function handleNewStatusFinish(e) {
    //     setNewStatusFinish(e.target.value)
    // }

    // function handleNewStatusCancel(e) {
    //     setNewStatusCancel(e.target.value)
    // }

    // function handleCancel() {
    //     for (let appointment in appointments){
    //         // const created = /created/i
    //         // appointment = appointment.status.replace(created, "canceled")
    //         appointment[0] = "canceled"
    //         return appointment
    //         }
    //     }


    // function handleFinish() {
    //     // if (appointment.status !== "finished")
    //     setFinish(finish)
    // }

    function handleFilterStatus() {
        setFilterStatus(filterStatus)
    }

    return (
        <>
            <div>
                <h1>Service Appointments</h1>
                <table className="table table-striped mt-5">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Reason</th>
                            <th scope="col">Customer</th>
                            <th scope="col">Technician</th>
                            <th scope="col">VIN</th>
                            <th scope="col">VIP</th>
                        </tr>
                    </thead>
                        {appointments
                        .filter((appointment) =>
                        appointment.status.includes(filterStatus)
                        )
                        .map(appointment => {
                            return(
                                <tbody key={appointment.id}>
                                    <tr>
                                        <td>{appointment.date}</td>
                                        <td>{appointment.time}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip}</td>
                                        <td>
                                            <button className="btn btn-success">Finish</button>
                                            <button className="btn btn-danger ml-3">Cancel</button>
                                        </td>
                                    </tr>
                                </tbody>
                            )
                        })}
                </table>
            </div>
        </>
    )


}

export default AppointmentList
