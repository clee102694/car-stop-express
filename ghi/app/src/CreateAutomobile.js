import { useEffect, useState } from 'react'


function CreateAutomobile() {
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setModel] = useState('')
    const [models, setModels] = useState([])

    const getData = async () => {
        const url = `http://localhost:8100/api/models/`
        const response = await fetch(url)


        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    useEffect(() =>{
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model = model

        const automobileUrl = `http://localhost:8100/api/automobiles`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(automobileUrl, fetchConfig)

        if (response.ok) {
            const newAutomobile = await response.json()
            setColor('')
            setYear('')
            setVin('')
            setModel('')
        }
    }

    const handleChangeColor = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleChangeYear = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleChangeVin = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleChangeModel = (event) => {
        const value = event.target.value
        setModel(value)
    }


    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Create an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeYear} value={year} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                            <label htmlFor="">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeVin} value={vin} placeholder="VIN" required type="vin" name="vin" id="vin" className="form-control" />
                            <label htmlFor="">VIN</label>
                        </div>
                        <div className="mb-3 mt-3">
                            <select onChange={handleChangeModel} value={model} required name="model" id="model" className="form-select">
                                <option value="">Choose a Model</option>
                                {models.map(model => {
                                    return(
                                        <option key={model} value={model}>{model.manufacturer.name} {model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Automobile</button>
                    </form>
                </div>
            </div>
        </>
    )
}
export default CreateAutomobile
