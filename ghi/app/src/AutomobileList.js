import { useEffect, useState } from 'react'


function AutomobileList() {
    const [autos, setAutos] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')

        if (response.ok) {
            const data = await response.json()
            setAutos(data.autos)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div>
                <h1>Automobiles</h1>
                <table className="table mt-5">
                    <thead>
                        <tr>
                            <th scope="col">Color</th>
                            <th scope="col">Year</th>
                            <th scope="col">VIN</th>
                            <th scope="col">Model</th>
                            <th scope="col">Availablity</th>
                        </tr>
                    </thead>
                        {autos.map(automobile => {
                            return (
                                <tbody key={automobile}>
                                    <tr>
                                        <td>{automobile.color}</td>
                                        <td>{automobile.year}</td>
                                        <td>{automobile.vin}</td>
                                        <td>{automobile.model.name}</td>
                                        <td>{automobile.sold}</td>
                                    </tr>
                                </tbody>
                            )
                        })}
                </table>
            </div>
        </>
    )
}

export default AutomobileList
