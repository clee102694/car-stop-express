import { useEffect, useState } from 'react'


function TechniciansList() {
    const [technicians, setTechnicians] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/')

        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }
    useEffect(()=>{
        getData()
    }, [])

    return (
        <>
            <div>
                <h1>Technicians</h1>
                <table className="table table-info table-striped table-bordered border-success mt-5">
                    <thead>
                        <tr>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Employee ID</th>
                        </tr>
                    </thead>
                        {technicians.map(technician => {
                            return(
                            <tbody className="" key={technician.id}>
                                <tr>
                                    <td>{technician.first_name}</td>
                                    <td>{technician.last_name}</td>
                                    <td>{technician.employee_id}</td>
                                </tr>
                            </tbody>
                       )
                })}
                </table>
            </div>
        </>
    )
}

export default TechniciansList;
