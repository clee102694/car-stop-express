import { useEffect, useState } from 'react'


function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [filterValue, setFilterValue] = useState("")
    const [filterClick, setFilterClick] = useState(true)

    const getData = async () => {
        const response = await fetch(`http://localhost:8080/api/appointments/`)

        if (response.ok) {
            const data = await response.json()
            setAppointments(data.appointments)
        }
    }
    useEffect(() => {
        getData()
    }, [])


    function handleFilterClick() {
        setFilterClick(filterClick)
    }

    function handleFilterChange() {
        if (filterClick) {
            setFilterValue(filterValue)
        }
    }

    return (
        <>
            <div>
                <h1>Service History</h1>
                <input onChange={handleFilterChange} />
                <button className="btn btn-success" onClick={handleFilterClick}>Search by VIN</button>
                <table className="table table-striped mt-5">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Reason</th>
                            <th scope="col">Customer</th>
                            <th scope="col">Technician</th>
                            <th scope="col">VIN</th>
                            <th scope="col">VIP</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                        {appointments
                        .filter((appointment) =>
                        filterClick && appointment.vin.includes(filterValue)
                        )
                        .map(appointment => {
                            return(
                                <tbody className="" key={appointment.id}>
                                    <tr>
                                        <td>{appointment.date}</td>
                                        <td>{appointment.time}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                </tbody>
                            )
                        })}
                </table>
            </div>
        </>
    )


}

export default ServiceHistory
