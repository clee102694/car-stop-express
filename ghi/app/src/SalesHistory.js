import { useEffect, useState } from 'react'


function SalesHistory() {
    const [sale, setSales] = useState([])
    const [filterValue, setFilterValue] = useState("")
    const [filterClick, setFilterClick] = useState(true)

    const getData = async () => {
        const response = await fetch(`http://localhost:8090/api/sales/`)

        if (response.ok) {
            const data = await response.json()
            setSales(data.sale)
        }
    }
    useEffect(() => {
        getData()
    }, [])


    function handleFilterClick() {
        setFilterClick(filterClick)
    }

    function handleFilterChange() {
        if (filterClick) {
            setFilterValue(filterValue)
        }
    }

    return (
        <>
            <div>
                <h1>Sales History</h1>
                <input onChange={handleFilterChange} />
                <button className="btn btn-success" onClick={handleFilterClick}>Search by VIN</button>
                <table className="table table-striped mt-5">
                    <thead>
                        <tr>
                            <th scope="col">Salesperson First and Last Name</th>
                            <th scope="col">Automobile VIN</th>
                            <th scope="col">Price</th>
                            <th scope="col">Customer First and Last Name</th>
                            <th scope="col">Technician</th>
                        </tr>
                    </thead>


                        {sale.map(sales => {
                                return (
                                <tbody className="" key={sales.id}>
                                    <tr>
                                        <td>{sales.salesperson.first_name} {sale.salesperson.last_name}</td>
                                        <td></td>
                                        <td>{sales.automobile.vin}</td>
                                        <td>{sales.price}</td>
                                        <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                                    </tr>
                                </tbody>
                            )
                        })}
                </table>
            </div>
        </>
    )


}

export default SalesHistory
