import { useEffect, useState } from 'react'


function CreateModel() {
    const [name, setName] = useState('')
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name
        data.picture_url = picture_url
        data.manufacturer = manufacturer

        const modelUrl = 'http://localhost:8100/api/models/'

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(modelUrl, fetchConfig)

        if (response.ok) {
            const newManufacturer = await response.json()
            setName('')
            setPictureUrl('')
            setManufacturer('')
        }
    }

    const handleChangeName = (event) => {
        const value = event.target.value
        setName(value)
    }

    const handleChangePictureUrl = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleChangeManufacturer = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    return (
        <>
            <div className="row">
                <h1>Create a Vehicle Model</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={name} placeholder="model name" required type="text" name="name" id="name" className="form-control" />
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleChangePictureUrl} value={picture_url} placeholder="Picture URL" required type="url" name="pic url" id="pic url" className="form-control" />
                    </div>
                    <div className="mb-3 mt-3">
                        <select onChange={handleChangeManufacturer} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option>Choose the Manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.name}>
                                        {manufacturer.name}
                                    </option>
                                )
                            })}
                        </select>

                    </div>
                    <button className="btn btn-primary">Create Car Model</button>
                </form>
            </div>
        </>
    )



}

export default CreateModel
