import { useEffect, useState } from 'react';


function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');

    if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople)
        console.log(data)
        console.log(salespeople)
    }
}

useEffect(()=>{
    getData()

}, [])

return (
    <>
    <div id="root"></div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Employee ID</th>
        </tr>
      </thead>
      <tbody>

        {salespeople.map(salesperson => {
            return (
                <tr key={salesperson.employee_id}>
              <td>{ salesperson.first_name }</td>
              <td>{ salesperson.last_name }</td>
              <td>{ salesperson.employee_id}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default SalespeopleList;


/* awaiting a promise to get or fetch data from the url, which
points to the salespeople API data I created
...
then uses the setSalespeople variable to useState function
with empty list parameter
takes an anonymous function and passes it into the getData function
which is all a paramter of useEffect.
The getData() does ...
salespeople, the variable I set to get the data from the
url I specified that points to the api data
/* There is only one default export function per page
{/* The line below performs the for loop iteration of
{/* useEffect, which is ...
/* set a new variable, data, that awaits the response, then
*/
