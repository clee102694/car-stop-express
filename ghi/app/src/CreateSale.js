
import { useEffect, useState } from 'react';


function CreateSale() {

    const [salesPerson, setSalesPerson] = useState('')
    const [automobile, setAutomobile] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.sale.salesperson.first_name = salesPerson
        data.sale.automobile.vin = automobile
        data.sale.customer.first_name = customer
        data.price = price

        const locationUrl = `http://localhost:8090/api/sales/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            const newSale = await response.json()
            setSalesPerson('')
            setAutomobile('')
            setCustomer('')
            setPrice('')
        }
    }

        const handleChangeSalesPerson = (event) => {
            const value = event.target.value
            setSalesPerson(value)
        }

        const handleChangeAutomobile = (event) => {
            const value = event.target.value
            setAutomobile(value)
        }

        const handleChangeCustomer = (event) => {
            const value = event.target.value
            setCustomer(value)
        }
        const handleChangePrice = (event) => {
            const value = event.target.value
            setPrice(value)
        }

    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Please fill out the options below to create a sale!!</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeSalesPerson} value={salesPerson} placeholder="Salesperson" required type="text" name="salesPerson" id="salesPerson" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeAutomobile} value={automobile} placeholder="Automobile VIN" required type="text" name="automobile" id="automobile" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeCustomer} value={customer} placeholder="Customer Name" required type="text" name="customer" id="customer" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangePrice} value={price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create Sale!</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateSale;
