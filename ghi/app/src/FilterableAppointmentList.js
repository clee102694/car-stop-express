import React from "react";
import { useState, useEffect } from "react"

// This was a "test page" to make sure I could get the filtering working

function FilterableAppointmentList() {
    const [appointments, setAppointments] = useState([])
    const [filterValue, setFilterValue] = useState("")

    async function getData() {
        const response = await fetch (`http://localhost:8080/api/appointments/`)
        const data = await response.json()
        console.log(data)
        setAppointments(data.appointments)
    }

    useEffect(() => {
        getData()
    }, [])

    function handleFilterChange(e) {
        setFilterValue(e.target.value)
    }

    return (
        <div>
            <h1>Appointments</h1>
            <input onChange={handleFilterChange} />
            <table>
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Time</th>
                        <th scope="col">Reason</th>
                        <th scope="col">Customer</th>
                        <th scope="col">Technician</th>
                        <th scope="col">VIN</th>
                        <th scope="col">VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                    .filter((appointment) =>
                        appointment.vin.includes(filterValue)
                    )
                    .map((appointment) => (
                    <tr key={appointment.id}>
                        <td>{appointment.date}</td>
                        <td>{appointment.time}</td>
                        <td>{appointment.reason}</td>
                        <td>{appointment.customer}</td>
                        <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                        <td>{appointment.vin}</td>
                        <td>{appointment.vip}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>

    )

}

export default FilterableAppointmentList
