import { useEffect, useState } from 'react'


function CreateTechnician() {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeID

        const locationUrl = `http://localhost:8080/api/technicians/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            const newTechnician = await response.json()
            setFirstName('')
            setLastName('')
            setEmployeeID('')
        }
    }

        const handleChangeFirstName = (event) => {
            const value = event.target.value
            setFirstName(value)
        }

        const handleChangeLastName = (event) => {
            const value = event.target.value
            setLastName(value)
        }

        const handleChangeEmployeeID = (event) => {
            const value = event.target.value
            setEmployeeID(value)
        }

    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Please fill out the options below to create a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeFirstName} value={firstName} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeLastName} value={lastName} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeEmployeeID} value={employeeID} placeholder="Employee ID" required type="number" name="employeeID" id="employeeID" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create Technician</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateTechnician;
