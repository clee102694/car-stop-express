import { useEffect, useState } from "react";

function SalesList() {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
            console.log(data);
            console.log(sales);

        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <div id="root">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Automobile</th>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.salesperson.id}</td>
                            <td>{sale.customer.id}</td>
                            <td>{sale.price}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </>
    );
}

export default SalesList;
