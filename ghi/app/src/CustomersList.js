import { useEffect, useState } from "react";

function CustomersList() {
    const [customer, setCustomers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/customers/');

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customer)
            console.log(data);
            console.log(customer);

        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <>
        <div id="root">
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                </tr>
            </thead>
            <tbody>
                {customer.map(customers => {
                    return (
                        <tr key={customers.phone_number}>
                            <td>{customers.first_name}</td>
                            <td>{customers.last_name}</td>
                            <td>{customers.address}</td>
                            <td>{customers.phone_number}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
        </>
    );
}

export default CustomersList;
