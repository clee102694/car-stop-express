import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateTechnician from './CreateTechnician';
import TechniciansList from './TechniciansList';
import CreateAppointment from './CreateAppointment';
import AppointmentList from './AppointmentList';
import SalespeopleList from './SalespeopleList'
import CreateSalesperson from './CreateSalesperson';
import ServiceHistory from './ServiceHistory';
import ManufacturerList from './ManufacturerList';
import AutomobileList from './AutomobileList';
import ModelList from './ModelList';
import CreateModel from './CreateModel';
import CreateManufacturer from './CreateManufacturer';
import CreateAutomobile from './CreateAutomobile';
import CustomersList from './CustomersList';
import FilterableAppointmentList from './FilterableAppointmentList';
import CreateCustomer from './CreateCustomer';
import SalesList from './SalesList';
import CreateSale from './CreateSale';
import SalesHistory from './SalesHistory';


function App(props) {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="models">
            <Route index element={<ModelList />} />
            <Route path="create" element={<CreateModel />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="create" element={<CreateAutomobile />} />
          </Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="create" element={<CreateManufacturer />} />
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="create" element={<CreateAppointment />} />
            <Route path="history" element={<ServiceHistory />} />
            <Route path="filter" element={<FilterableAppointmentList />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />} />
            <Route path="create" element={<CreateTechnician />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople">
            <Route index element={<SalespeopleList/>} />
            <Route path="create" element={<CreateSalesperson/>} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomersList/>} />
            <Route path="create" element={<CreateCustomer/>}/>
          </Route>
          <Route path="sales">
            <Route index element={<SalesList/>}/>
            <Route path="create" element={<CreateSale/>}/>
            <Route path="history" element={<SalesHistory/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;

{/* <Route component={PageNotFound}/> */}


/*
<Route path="create" element={<CreateSalesperson/>}/>

        <Route path="customers">
          <Route index element={<CustomersList/>}/>
          <Route path="create" element={<CreateCustomer/>}/>
        </Route>
        <Route path="sales">
          <Route index element={<SalesList/>}/>
          <Route path="create" element={<CreateSale/>}/>
        </Route>


*/
