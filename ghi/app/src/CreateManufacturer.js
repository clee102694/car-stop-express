import { useEffect, useState } from 'react'


function CreateManufacturer() {
    const [name, setName] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.name = name

        const url = `http://localhost:8100/api/manufacturers/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            const newManufacturer = await response.json()
            setName('')

        }
    }

        const handleChangeName = (event) => {
            const value = event.target.value
            setName(value)

        }



    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeName} value={name} placeholder="Manufacturer Name" required type="text" name="name" id="name" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateManufacturer
