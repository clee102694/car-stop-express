import { useEffect, useState } from 'react';


function CreateSalesperson() {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeID

        const locationUrl = `http://localhost:8090/api/salespeople/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            const newSalesperson = await response.json()
            setFirstName('')
            setLastName('')
            setEmployeeID('')
        }
    }

        const handleChangeFirstName = (event) => {
            const value = event.target.value
            setFirstName(value)
        }

        const handleChangeLastName = (event) => {
            const value = event.target.value
            setLastName(value)
        }

        const handleChangeEmployeeID = (event) => {
            const value = event.target.value
            setEmployeeID(value)
        }

    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Please fill out the options below to create a salesperson!</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeFirstName} value={firstName} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeLastName} value={lastName} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeEmployeeID} value={employeeID} placeholder="Employee ID" required type="number" name="employeeID" id="employeeID" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create Salesperson</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateSalesperson;












/*
function CreateSalesperson() {

   // const [salespeople, setSalesperson] = useState([])
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const [employeeRegistered, setEmployeeRegistered] = useState(false)

    // const getData = async () => {
    //     const url = 'http://localhost:8090/api/salespeople/';
    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         setSalesperson(data.salespeople)
    //         console.log()
    //     }
    // }

    // useEffect(() => {
    //     getData();
    // }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const another_url = 'http://localhost:8090/api/salespeople/';

        const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(another_url, fetchConfig);

        if (response.ok) {
          setFormData({
            name: '',
            email: '',
            conference: ''
        });

        setEmployeeRegistered(true);
    }
}
    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputFirstName = e.target.first_name;
        const inputLastName = e.target.last_name;
        setFormData({
            ...formData,
            [inputFirstName]:value,
            [inputLastName]:value
        });
        }
    const formClasses = (!employeeRegistered) ? '': 'd-none';
    const messageClasses = (!employeeRegistered) ? 'alert alert-success d-none mb-0': 'alert alert-success mb-0';

    return (
        <div>
        <div>
          <div>
          </div>

          <div>
            <div>
              <div>

                <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">

                  <p>
                    Register as a salesperson!
                  </p>

                  <div className="row">
                    <div className="col">
                      <div>
                        <input onChange={handleChangeName} required placeholder="Your first name" type="text" id="first_name" name="first_name" className="form-control" />
                        <label htmlFor="name">Your first name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleChangeName} required placeholder="Your last name name" type="text" id="last_name" name="last_name" className="form-control" />
                        <label htmlFor="name">Your last name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleChangeName} required placeholder="Your employee ID" type="text" id="employee_id" name="employee_id" className="form-control" />
                        <label htmlFor="email">Your employee ID </label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm registered!</button>
                </form>

                <div className={messageClasses} id="success-message">
                  Congratulations! You're all signed up!
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    )
}

export default CreateSalesperson;


/*

import React, {useState, useEffect } from 'react';

function AttendConferenceForm() {
  const [conferences, setConferences] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    conference: ''
  })

  const [hasSignedUp, setHasSignedUp] = useState(false)

  const getData = async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8001${formData.conference}attendees/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        name: '',
        email: '',
        conference: ''
      });

      setHasSignedUp(true);
    }
  }

  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  //Allows for the form to display on when a user has not signed in.
  //If a user has signed in, the css class d-none is added to the unneeded piece of the interface

  const formClasses = (!hasSignedUp) ? '' : 'd-none';
  const messageClasses = (!hasSignedUp) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5">
      <div className="row">
        <div className="col col-sm-auto">
          <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="logo.svg" />
        </div>

        <div className="col">
          <div className="card shadow">
            <div className="card-body">

              <form className={formClasses} onSubmit={handleSubmit} id="create-attendee-form">
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>

                <div className="mb-3">
                  <select onChange={handleChangeName} name="conference" id="conference" required>
                    <option value="">Choose a conference</option>
                    {
                      conferences.map(conference => {
                        return (
                          <option key={conference.href} value={conference.href}>{conference.name}</option>
                        )
                      })
                    }
                  </select>
                </div>

                <p className="mb-3">
                  Now, tell us about yourself.
                </p>

                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>

              <div className={messageClasses} id="success-message">
                Congratulations! You're all signed up!
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendConferenceForm;

*/
