import { useEffect, useState } from 'react'


function CreateAppointment() {
    const [date, setDate] = useState('')
    const [time, setTime] = useState('')
    const [reason, setReason] = useState('')
    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [technician, setTechnician] = useState('')
    const [technicians, setTechnicians] = useState([])

    const getData = async () => {
        const url = `http://localhost:8080/api/technicians/`
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.date = date
        data.time = time
        data.reason = reason
        data.vin = vin
        data.customer = customer
        data.technician = technician

        const appointmentUrl = `http://localhost:8080/api/appointments/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        }

        const response = await fetch(appointmentUrl, fetchConfig)

        if (response.ok) {
            const newAppointment = await response.json()
            setDate('')
            setTime('')
            setReason('')
            setVin('')
            setCustomer('')
            setTechnician('')
        }
    }

    const handleChangeDate = (event) => {
        const value = event.target.value
        setDate(value)
    }

    const handleChangeTime = (event) => {
        const value = event.target.value
        setTime(value)
    }

    const handleChangeReason = (event) => {
        const value = event.target.value
        setReason(value)
    }

    const handleChangeVin = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleChangeCustomer = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handleChangeTechnician = (event) => {
        const value = event.target.value
        setTechnician(value)
    }

    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Please fill out the options below to create an appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeCustomer} value={customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                            <label htmlFor="">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeVin} value={vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="">VIN</label>
                        </div>
                        <div className="mb-3">
                            <textarea onChange={handleChangeReason} value={reason} placeholder="Reason for Appointment" rows="3" required type="text" name="reason" id="reason" className="form-control" />
                        </div>
                        <div  className="mb-3">
                            <input onChange={handleChangeDate} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                        </div>
                        <div className="mb-3">
                            <input onChange={handleChangeTime} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                        </div>
                        <div className="mb-3 mt-3">
                            <select onChange={handleChangeTechnician} value={technician} required name="technician" id="technician" className="form-select">
                                <option>Choose your Technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.employee_id}>{technician.first_name}  {technician.last_name}</option>
                                    )
                                })}
                            </select>
                            
                        </div>
                        <button className="btn btn-primary">Create Appointment</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateAppointment
