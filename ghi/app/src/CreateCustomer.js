import { useEffect, useState } from 'react';


function CreateCustomer() {

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [customerAddress, setCustomerAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')


    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}
        data.first_name = firstName
        data.last_name = lastName
        data.address = customerAddress
        data.phone_number = phoneNumber

        const locationUrl = `http://localhost:8090/api/customers/`

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationUrl, fetchConfig)

        if (response.ok) {
            const newSalesperson = await response.json()
            setFirstName('')
            setLastName('')
            setCustomerAddress('')
            setPhoneNumber('')
        }
    }

        const handleChangeFirstName = (event) => {
            const value = event.target.value
            setFirstName(value)
        }

        const handleChangeLastName = (event) => {
            const value = event.target.value
            setLastName(value)
        }

        const handleChangeCustomerAddress = (event) => {
            const value = event.target.value
            setCustomerAddress(value)
        }
        const handleChangePhoneNumber = (event) => {
            const value = event.target.value
            setPhoneNumber(value)
        }


    return (
        <>
            <div className="row">
                <div className="offset-2 col-6">
                    <h1>Please fill out the options below to create a customer!</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeFirstName} value={firstName} placeholder="Your first name here" required type="text" name="firstName" id="firstName" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeLastName} value={lastName} placeholder="Your last name here" required type="text" name="lastName" id="lastName" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangeCustomerAddress} value={customerAddress} placeholder="123 Your Address Here, Anywhere, YO 90210" required type="text" name="customerAddress" id="customerAddress" className="form-control" />
                        </div>
                        <div className="form-floatting mb-3">
                            <input onChange={handleChangePhoneNumber} value={phoneNumber} placeholder="Phone Number" required type="number" name="phoneNumber" id="phoneNumber" className="form-control" />
                        </div>
                        <button className="btn btn-primary">Create Customer</button>
                    </form>
                </div>
            </div>
        </>
    )
}

export default CreateCustomer;
