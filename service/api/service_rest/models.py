from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    employee_id = models.PositiveSmallIntegerField(unique=True)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length = 17)
    sold = models.BooleanField(default=False)

class Appointment(models.Model):
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.CharField(max_length = 150)
    status = models.CharField(max_length = 30, default="created")
    vin = models.CharField(max_length = 17)
    customer = models.CharField(max_length = 150)
    vip = models.CharField(max_length = 30, default="Not VIP")

    technician = models.ForeignKey(
        Technician,
        related_name="technicians",
        on_delete=models.CASCADE,
    )
